# Reveal.js Explicit Link

When using reveal.js for presentation, it is often annoying to type explicit links: links whose text
and link are the same. For example, to display an explicit link that points to
https://www.topbug.net/revealjs-explicit-link, we need to type the code

    <a href="https://www.topbug.net/revealjs-explicit-link">https://www.topbug.net/revealjs-explicit-link</a>

which is quite redundant. This reveal.js plugin solves the problem by simplifying the code above to

    <a class="explicit-link">https://www.topbug.net/revealjs-explicit-link</a>

## Installation

To install it, in your reveal.js presentation, download the plugin and copy the file
`explicit-link.js` to the `plugin/explicit-link/` directory, or run the following command:

    git clone https://gitlab.com/xuhdev/revealjs-explicit-link plugin/explicit-link

Then in your presentation file (usually `index.html`), add the plugin as a dependency:

    Reveal.initialize({
        dependencies: [
            // Other dependencies...
            { src: 'plugin/explicit-link/explicit-link.js' },
        ]
    });

## Usage

As previously showed, assign an `a` tag the `explicit-link` class will have it an explicit link:

     <a class="explicit-link">http://example.org</a>

## Development

The development happens on [GitLab][]. Please report any bugs and feature requests to
the [issue tracker][].

## License

[MIT license][]:

        Copyright (c) 2016 Hong Xu <hong@topbug.net>

        Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
        associated documentation files (the "Software"), to deal in the Software without restriction,
        including without limitation the rights to use, copy, modify, merge, publish, distribute,
        sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all copies or
        substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
        NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
        NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
        DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[GitLab]: https://gitlab.com/xuhdev/revealjs-explicit-link
[MIT license]: https://opensource.org/licenses/MIT
[issue tracker]: https://gitlab.com/xuhdev/revealjs-explicit-link/issues
